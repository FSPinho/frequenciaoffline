var currentTeams = [];

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('of-checkbox-top').addEventListener('click', onCheckAll);
    document.getElementById('of-delete-selected').addEventListener('click', onDeleteSelected);

    doInit();    
});

function doInit() {
	getTeams(function(teams) {
		currentTeams = teams;
		var table = document.getElementById("teams-table");
		var fCell = table.rows[0].cells[0];

		fCell.getElementsByClassName("mdl-checkbox__input")[0].setAttribute("id", "checkbox-top");

		for(var i = 0; i < teams.length; i++) {
			var row = table.insertRow(i + 1);

			var cell1 = row.insertCell(0);

			cell1.classList.add("mdl-data-table__cell--non-numeric");
			cell1.classList.add("mdl-js-ripple-effect");
			cell1.classList.add("custon-content");
			
			var ripple = document.createElement('span');
			ripple.classList.add("mdl-ripple");
			cell1.appendChild(ripple);

			var link = document.createElement("a");
			link.classList.add("mdl-color-text--grey-700");
			link.href="#";
			link.style.textDecoration = "none";
			link.style.height = "100%";
			link.id = teams[i].id;
			cell1.id = teams[i].id;
			link.addEventListener('click', function(e) { onSeeTeam(e.srcElement.id); }, false);
			cell1.addEventListener('click', function(e) { onSeeTeam(e.srcElement.id); }, false);
			link.innerHTML = teams[i].name;
			cell1.appendChild(link);

			var cell2 = row.insertCell(0);
			var htmlString = "<label class=\"mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect\" for=\"checkbox-" + teams[i].id + "\"> <input type=\"checkbox\" id=\"checkbox-" + teams[i].id + "\" class=\"mdl-checkbox__input\"/> </label>";
			cell2.innerHTML = htmlString;
		}

		setTimeout(function() {
          	componentHandler.upgradeDom();
        }, 0);
	});
}

function onCheckAll() {
	var table = document.getElementById("teams-table");
	
	var fCell = table.rows[0].cells[0];
	value = hasClass(fCell.getElementsByClassName("mdl-checkbox")[0], "is-checked");
	checked = fCell.getElementsByClassName("mdl-checkbox__input")[0].checked;

	for(var i = 1; i < table.rows.length; i++) {
		console.log("On check all: " + checked);
		var row = table.rows[i];
		var cell = row.cells[0];
		var cbl = cell.getElementsByClassName("mdl-checkbox")[0];
		var cbi = cell.getElementsByClassName("mdl-checkbox__input")[0];

		if(value) {
			if(cbl.className.indexOf("is-checked") == -1)
				cbl.className = cbl.className + " is-checked";
			cbi.checked = true;
		}
		else {
			cbl.className = cbl.className.replace(" is-checked", "");
			cbi.checked = false;
		}
	}

	setTimeout(function() {
      	componentHandler.upgradeDom();
    }, 0);
}

function onDeleteSelected() {
	var table = document.getElementById("teams-table");
	
	var idsToRemove = [];

	for(var i = 1; i < table.rows.length; i++) {
		var row = table.rows[i];
		var cell = row.cells[0];
		var checked = cell.getElementsByClassName("mdl-checkbox__input")[0].checked;
		var id = cell.getElementsByClassName("mdl-checkbox__input")[0].id.replace("checkbox-", "");

		if(checked) {
			table.deleteRow(i--);
			idsToRemove.push(id);
		}
	}

	deleteTeam(idsToRemove);

	setTimeout(function() {
      	componentHandler.upgradeDom();
    }, 0);
}

function deleteTeam(ids) {
	for(id of ids) {
		removeSClassFromTeam(id, function() {
			removeStudentsFromTeam(id, function() {
				removeClassFromTeam(id, function() {
					removeTeam(id, function() {});
				});
			});
			
		});
	}
}

function hasId(id, ids) {
	for(var i = 0; i < ids.length; i++) {
		if(id == ids[i]) return true;
	}
	return false;
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function onSeeTeam(id) {
	log("See team: " + id);
	chrome.storage.sync.set({'tmpTeamToSee': id}, function() {
        chrome.tabs.getCurrent(function (tab) {
			chrome.tabs.update(tab.id, {url: chrome.extension.getURL('seeteam.html')}, function(tab) {});
		});
    });
	
}