var studentsReceived = false;
var classReceived = false;

var students = null;
var mClass = null;

chrome.runtime.onMessage.addListener(function(request, sender) {
	console.log("Message received");
	if (request.action == "getSourceStudents" && !studentsReceived) {
		studentsReceived = true;
		students = JSON.parse(request.source);
		console.log("students received!");
	}

	if (request.action == "getSourceClass" && !classReceived) {
		classReceived = true;
		mClass = JSON.parse(request.source);
		console.log("class received!");
	}

	if(studentsReceived && classReceived) 
		doAutoFill();
});

function doAutoFill() {
	var inputDiario = document.getElementById("taDiario");
	inputDiario.value = mClass.diary;

	var inputDate = document.getElementById("date1");
	inputDate.value = mClass.date;

	var table = document.getElementsByClassName("tabela_ver_freq")[0];
	boxes = table.getElementsByTagName("input");
	for(var i = 1; i < boxes.length; i++) {
		var reg = table.rows[i].cells[2].innerHTML;
		
		var toCheck = false;
		for(var j = 0; j < students.length; j++) {
			toCheck |= (students[j].registration == reg);
		}

		boxes[i].checked = toCheck;
	}
}

