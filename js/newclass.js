var curentClass = null;
var currentTeam = null;
var currentStudents = [];

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('of-save-team').addEventListener('click', onAddClass);
    document.getElementById('of-cancel-team').addEventListener('click', onSeeTeam);
    document.getElementById('of-checkbox-top').addEventListener('click', onCheckAll);
    document.getElementById('of-see-teams').addEventListener('click', onSeeTeams);

    doInit();    
});

function doInit() {
	hideProgess();
	chrome.storage.sync.get(null, function(data) {
		teamId = data["tmpTeamToFrequency"];
		classId = data["tmpClassToFrequency"];
		onLoadClass(teamId, classId);
	});
}

function onLoadClass(teamId, classId) {
	var callBacks = 3;
	var callBack = function() {
		callBacks--;
		if(callBacks == 0) updateCheckList();
	}

	log("loading class: " + classId);
	getClass(classId, function(mClass) {
		log("class: " + mClass);
		if(mClass == null) {
			mClass = new Class(-1, getDate(), "", "", true, teamId);
		}

		currentClass = mClass;
		document.getElementById("class-plane").value = mClass.plane;
		document.getElementById("class-diary").value = mClass.diary;
		document.getElementById("class-date").value = mClass.date;
		
		setTimeout(function() {
          	componentHandler.upgradeDom();
        }, 0);

        callBack();
	});

	getTeam(teamId, function(team) {
		currentTeam = team;
		var name = document.getElementById("team-title");
		name.innerHTML = team.name;

		setTimeout(function() {
          	componentHandler.upgradeDom();
        }, 0);

        callBack();
	});

	getStudentsFromTeam(teamId, function(students) {
		currentStudents = students;
		var table = document.getElementById("teams-table");
		var fCell = table.rows[0].cells[0];

		fCell.getElementsByClassName("mdl-checkbox__input")[0].setAttribute("id", "checkbox-top");

		for(var i = 0; i < students.length; i++) {
			var row = table.insertRow(i + 1);

			var cell1 = row.insertCell(0);
			cell1.classList.add("mdl-data-table__cell--non-numeric");
			cell1.innerHTML = students[i].name;

			var cell2 = row.insertCell(0);
			cell2.classList.add("mdl-data-table__cell--non-numeric");
			cell2.innerHTML = students[i].registration;

			var cell2 = row.insertCell(0);
			var htmlString = "<label id=\"checkbox-div-" + students[i].id + "\" class=\"mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect\" for=\"checkbox-" + students[i].id + "\"> <input type=\"checkbox\" id=\"checkbox-" + students[i].id + "\" class=\"mdl-checkbox__input\"/> </label>";
			cell2.innerHTML = htmlString;
		}

		setTimeout(function() {
          	componentHandler.upgradeDom();
        }, 0);

        callBack();
	});
}

function updateCheckList() {
	getSClassFromClass(currentClass.id, function(sClasses) {
		console.log(sClasses.length);
		for(sc of sClasses) {
			var checkboxDiv = document.getElementById("checkbox-div-" + sc.idStudent);
			checkboxDiv.classList.add("is-checked");

			var checkbox = document.getElementById("checkbox-" + sc.idStudent);
			checkbox.checked = true;
		}
	});
}

function onCheckAll() {
	var table = document.getElementById("teams-table");
	
	var fCell = table.rows[0].cells[0];
	//value = hasClass(fCell.getElementsByClassName("mdl-checkbox")[0], "is-checked");
	value = document.getElementById("checkbox-top").checked;
	checked = fCell.getElementsByClassName("mdl-checkbox__input")[0].checked;

	for(var i = 1; i < table.rows.length; i++) {
		console.log("On check all: " + checked);
		var row = table.rows[i];
		var cell = row.cells[0];
		var cbl = cell.getElementsByClassName("mdl-checkbox")[0];
		var cbi = cell.getElementsByClassName("mdl-checkbox__input")[0];

		if(value) {
			if(cbl.className.indexOf("is-checked") == -1)
				cbl.classList.add("is-checked");
				//cbl.className = cbl.className + " is-checked";
			cbi.checked = true;
		}
		else {
			cbl.classList.remove("is-checked");
			//cbl.className = cbl.className.replace(" is-checked", "");
			cbi.checked = false;
		}
	}

	setTimeout(function() {
      	componentHandler.upgradeDom();
    }, 0);
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function onSeeTeam() {
	log("On see team");
	chrome.storage.sync.set({'tmpTeamToSee': currentTeam.id}, function() {
        chrome.tabs.getCurrent(function (tab) {
			chrome.tabs.update(tab.id, {url: chrome.extension.getURL('seeteam.html')}, function(tab) {});
		});
    });
	
}

function onAddClass() {
	seeProgess();
	log("Add class: " + currentStudents.length + " students");

	currentClass.plane = document.getElementById("class-plane").value;
	currentClass.diary = document.getElementById("class-diary").value;
	currentClass.date = document.getElementById("class-date").value;

	var oldClassId = currentClass.id;
	removeClass(currentClass.id, function() {
		saveClass(currentClass, function() {
			var callBacks = currentStudents.length;
			var callBack = function() {
				callBacks--;
				if(callBacks == 0) {
					hideProgess();
					showToast("Frequência adicionada!", function() {
						onSeeTeam();
					});
					
				}
			}

			removeSClass(oldClassId, function() {
				for(s of currentStudents) {
					var checkbox = document.getElementById("checkbox-" + s.id);
					var checked = checkbox.checked;

					var sid = s.id;
					if(checked) {
						sc = new StudentClass(sid, currentClass.id);
						saveSClass(sc, callBack);
					} else {
						callBack();
					}
				}
			});
		});
	})

}

function seeProgess() {
	var bar = document.getElementById("of-progress-bar");
	var done = document.getElementById("of-save-team");
	var cancel = document.getElementById("of-cancel-team");
	bar.style.display = "block";
	done.style.display = "none";
	cancel.style.display = "none";
}

function hideProgess() {
	var bar = document.getElementById("of-progress-bar");
	var done = document.getElementById("of-save-team");
	var cancel = document.getElementById("of-cancel-team");
	bar.style.display = "none";
	done.style.display = "block";
	cancel.style.display = "block";
}

function onSeeTeams() {
	log("Do see teams");
    chrome.tabs.update({'url': chrome.extension.getURL('allteams.html')}, function(tab) {});
}

function getDate() {
	var date = new Date();
	var day = date.getDate();
	var monthIndex = date.getMonth() + 1;
	var year = date.getFullYear();

	return (day + '/' + monthIndex + '/' + year);
}