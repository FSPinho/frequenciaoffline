function Student(oId, oRegistration, oName, oIdTeam) {
    this.id = oId;
    this.registration = oRegistration;
    this.name = oName;
    this.idTeam = oIdTeam;

    this.toString = function() {
        return "Student: " + this.id + ", " + this.registration + ", " + this.name + ", " + this.idTeam;
    }
}

function DOMtoString(doc) {
    var oTable = doc.getElementsByClassName('tabela_ver_freq')[0];

    students = [];

    //gets rows of table
    var rowLength = oTable.rows.length;

    //loops through rows    
    for (i = 1; i < rowLength; i++){

        //gets cells of current row
        var oCells = oTable.rows.item(i).cells;

        // registration
        var name = oCells.item(1).innerHTML;
        var registration = oCells.item(2).innerHTML;

        students.push(new Student(-1, registration, name, -1));
    }

    return JSON.stringify(students);
}

function getTeamName(doc) {  
    var div = doc.getElementById('info');
    var h1 = div.getElementsByTagName('h1')[0];

    return h1.innerHTML;
}

console.log("Sending message");

chrome.runtime.sendMessage({
    action: "getSource",
    source: DOMtoString(document)
});

chrome.runtime.sendMessage({
    action: "getSourceName",
    source: getTeamName(document)
});