KEY_STUDENT = "STUDENT";
KEY_TEAM = "TEAM";
KEY_CLASS = "CLASS";
KEY_STUDENT_CLASS = "STUDENT_CLASS";

var db = null;

document.addEventListener('DOMContentLoaded', function () {
	onOpenDatabase();
});

function onOpenDatabase(){
    if (window.openDatabase) {
        log("Este browser suporta Web SQL Databases");
    }
    else {
        log("Este browser suporta NÃO Web SQL Databases");
    }

    db = openDatabase('offlinefrequency', '1.0', 'offlinefrequency', 10 * 1024 * 1024);
    onCreateTables();
}

function onCreateTables() {
	db.transaction( function (tx) {
		tx.executeSql(
			"CREATE TABLE IF NOT EXISTS " + KEY_STUDENT + " (id integer primary key autoincrement, name, registration, idTeam)", 
			[], 
			function() {
				log("Table " + KEY_STUDENT + " created!");
			}, 
			function() {
				log("Table " + KEY_STUDENT + " not created!");
			}
		);
	});

	db.transaction( function (tx) {
		tx.executeSql(
			"CREATE TABLE IF NOT EXISTS " + KEY_TEAM + " (id integer primary key autoincrement, name)", 
			[], 
			function() {
				log("Table " + KEY_TEAM + " created!");
			}, 
			function() {
				log("Table " + KEY_TEAM + " not created!");
			}
		);
	});

	db.transaction( function (tx) {
		tx.executeSql(
			"CREATE TABLE IF NOT EXISTS " + KEY_CLASS + " (id integer primary key autoincrement, date, plane, diary, presence, idTeam)", 
			[], 
			function() {
				log("Table " + KEY_CLASS + " created!");
			}, 
			function() {
				log("Table " + KEY_CLASS + " not created!");
			}
		);
	});

	db.transaction( function (tx) {
		tx.executeSql(
			"CREATE TABLE IF NOT EXISTS " + KEY_STUDENT_CLASS + " (idStudent, idClass)", 
			[], 
			function() {
				log("Table " + KEY_STUDENT_CLASS + " created!");
			}, 
			function() {
				log("Table " + KEY_STUDENT_CLASS + " not created!");
			}
		);
	});
}

// Log

function log(message) {
	console.log(message);
}

// AllStorage

function clearStorage() {
	chrome.storage.sync.clear(function() {
		log("All store removed!");
	});
}

function verifyAvaliableData() {
	chrome.storage.sync.getBytesInUse(null, function(bytesInUse) {
		log((102400 - bytesInUse) + " bytens available, " + bytesInUse + " in use!");
	});
}

// Default id 
function getNewId(callBackFunction) {
	chrome.storage.sync.get("defaultId", function(data) {
		var id = 0;
		if(Object.keys(data).length > 0) {
			id = parseInt(data["defaultId"]);
			id++;	
		}

		chrome.storage.sync.set({'defaultId': id}, function() {});

		callBackFunction(id);
	});
}

// Student

function Student(oId, oRegistration, oName, oIdTeam) {
	this.id = oId;
	this.registration = oRegistration;
	this.name = oName;
	this.idTeam = oIdTeam;

	this.toString = function() {
		return "Student: " + this.id + ", " + this.registration + ", " + this.name + ", " + this.idTeam;
	}
}

function saveStudent(student, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'INSERT INTO ' + KEY_STUDENT + ' (name, registration, idTeam) VALUES(?, ?, ?)', 
			[student.name, student.registration, student.idTeam], 
			function(tx, results) {
				student.id = results.insertId;
				log("Insert " + KEY_STUDENT + "!");
				callBackFunction();
			}, 
			function(tx, error) {
				log("Insert " + KEY_STUDENT + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

function saveStudents(students, callBackFunction) {
	var callBacks = students.length;
	var callBack = function() {
		callBacks--;
		if(callBacks == 0 && callBackFunction != undefined) callBackFunction();
	}

	for(var i = 0; i < students.length; i++) {
		saveStudent(students[i], callBack);
	}
}

function getStudents(callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_STUDENT, 
			[], 
			function(tx, results) {
				log("Select " + KEY_STUDENT + "!");
				students = [];
				for(var i = 0; i < results.rows.length; i++) {
					student = results.rows.item(i);
					students.push(student);
				}

				callBackFunction(students);
			}, 
			function(tx, error) {
				log("Select " + KEY_STUDENT + " error: " + error.message);
				callBackFunction([]);
			}
		);
	});
}

function getStudentsFromTeam(teamId, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_STUDENT + ' WHERE idTeam = ' + teamId, 
			[], 
			function(tx, results) {
				log("Select " + KEY_STUDENT + " from " + KEY_TEAM + "_" + teamId + "!");
				students = [];
				for(var i = 0; i < results.rows.length; i++) {
					student = results.rows.item(i);
					students.push(student);
				}

				callBackFunction(students);
			}, 
			function(tx, error) {
				log("Select " + KEY_STUDENT + " error: " + error.message);
				callBackFunction([]);
			}
		);
	});
}

function removeStudentsFromTeam(idTeam, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'DELETE FROM ' + KEY_STUDENT + ' WHERE idTeam = ' + id, 
			[], 
			function(tx, results) {
				log("Remove " + KEY_STUDENT + " from team!");
				callBackFunction(true);
			}, 
			function(tx, error) {
				log("Remove " + KEY_STUDENT + " from team error: " + error.message);
				callBackFunction(false);
			}
		);
	});
}

function removeStudents(callBackFunction) {
	chrome.storage.sync.remove("students", function() {});
	callBackFunction();
}

// Class

function Class(oId, oDate, oPlane, oDiary, oPresence, oIdTeam) {
	this.id = oId;
	this.date = oDate;
	this.plane = oPlane;
	this.diary = oDiary;
	this.presence = oPresence;
	this.idTeam = oIdTeam;

	this.toString = function() {
		return "Class: " + this.id + ", " + this.name;
	}
}

function saveClass(mClass, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'INSERT INTO ' + KEY_CLASS + ' (date, plane, diary, presence, idTeam) VALUES(?, ?, ?, ?, ?)', 
			[mClass.date, mClass.plane, mClass.diary, mClass.presence, mClass.idTeam], 
			function(tx, results) {
				mClass.id = results.insertId;
				log("Insert " + KEY_CLASS + "!");
				callBackFunction();
			}, 
			function(tx, error) {
				log("Insert " + KEY_CLASS + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

function getClass(id, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_CLASS + ' WHERE id = ' + id, 
			[], 
			function(tx, results) {
				log("Select " + KEY_CLASS + "_" + id + "!");
				if(results.rows.length > 0) {
					callBackFunction(results.rows.item(0));
				} else {
					callBackFunction(null);
				}
			}, 
			function(tx, error) {
				log("Select " + KEY_CLASS + " error: " + error.message);
				callBackFunction(null);
			}
		);
	});
}

function getClassesFromTeam(teamId, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_CLASS + ' WHERE idTeam = ' + teamId, 
			[], 
			function(tx, results) {
				log("Select " + KEY_CLASS + " from " + KEY_TEAM + "_" + teamId + "!");
				classes = [];
				for(var i = 0; i < results.rows.length; i++) {
					mClass = results.rows.item(i);
					classes.push(mClass);
				}

				callBackFunction(classes);
			}, 
			function(tx, error) {
				log("Select " + KEY_CLASS + " error: " + error.message);
				callBackFunction([]);
			}
		);
	});
}

function removeClass(id, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'DELETE FROM ' + KEY_CLASS + ' WHERE id = ' + id, 
			[], 
			function(tx, results) {
				log("Delete " + KEY_CLASS + "_" + id + "!");
				
				callBackFunction();
			}, 
			function(tx, error) {
				log("Delete " + KEY_CLASS + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

function removeClassFromTeam(idTeam, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'DELETE FROM ' + KEY_CLASS + ' WHERE idTeam = ' + id, 
			[], 
			function(tx, results) {
				log("Delete " + KEY_CLASS + "_" + idTeam + "!");
				
				callBackFunction();
			}, 
			function(tx, error) {
				log("Delete " + KEY_CLASS + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

// Team

function Team(oId, oName) {
	this.id = oId;
	this.name = oName;

	this.toString = function() {
		return "Team: " + this.id + ", " + this.name;
	}
}

function saveTeam(team, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'INSERT INTO ' + KEY_TEAM + ' (name) VALUES(?)', 
			[team.name], 
			function(tx, results) {
				team.id = results.insertId;
				log("Insert " + KEY_TEAM + "!");
				callBackFunction();
			}, 
			function(tx, error) {
				log("Insert " + KEY_TEAM + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

function saveTeams(teams, callBackFunction) {
	var callBacks = teams.length;
	var callBack = function() {
		callBacks--;
		if(callBacks == 0 && callBackFunction != undefined) callBackFunction();
	}

	for(var i = 0; i < teams.length; i++) {
		saveTeam(teams[i], callBack);
	}
}

function getTeam(id, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_TEAM + ' WHERE ID = ' + id, 
			[], 
			function(tx, results) {
				var team = results.rows.item(0);
				log("Select " + KEY_TEAM + "_" + team.id + "!");
				callBackFunction(team);
			}, 
			function(tx, error) {
				log("Select " + KEY_TEAM + " error: " + error.message);
				callBackFunction(null);
			}
		);
	});
}

function getTeamFromName(name, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_TEAM + ' WHERE name = "' + name + '"', 
			[], 
			function(tx, results) {
				var team = results.rows.item(0);
				log("Select " + KEY_TEAM + "_" + team.id + "!");
				callBackFunction(team);
			}, 
			function(tx, error) {
				log("Select " + KEY_TEAM + " error: " + error.message);
				callBackFunction(null);
			}
		);
	});
}


function getTeams(callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_TEAM, 
			[], 
			function(tx, results) {
				log("Select " + KEY_TEAM + "!");
				teams = [];
				for(var i = 0; i < results.rows.length; i++) {
					team = results.rows.item(i);
					teams.push(team);
				}

				callBackFunction(teams);
			}, 
			function(tx, error) {
				log("Select " + KEY_TEAM + " error: " + error.message);
				callBackFunction([]);
			}
		);
	});
}

function removeTeam(id, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'DELETE FROM ' + KEY_TEAM + ' WHERE id = ' + id, 
			[], 
			function(tx, results) {
				log("Remove " + KEY_TEAM + "!");
				callBackFunction(true);
			}, 
			function(tx, error) {
				log("Remove " + KEY_TEAM + " error: " + error.message);
				callBackFunction(false);
			}
		);
	});
}

// Student Class Relation

function StudentClass(oIdStudent, oIdClass) {
	this.idStudent = oIdStudent;
	this.idClass = oIdClass;

	this.toString = function() {
		return "StudentClass: " + this.idStudent + ", " + this.idClass;
	}
}

function saveSClass(sClass, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'INSERT INTO ' + KEY_STUDENT_CLASS + ' (idStudent, idClass) VALUES(?, ?)', 
			[sClass.idStudent, sClass.idClass], 
			function(tx, results) {
				sClass.id = results.insertId;
				log("Insert " + KEY_STUDENT_CLASS + "!");
				callBackFunction();
			}, 
			function(tx, error) {
				log("Insert " + KEY_STUDENT_CLASS + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}

function getSClassFromStudent(idStudent, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_STUDENT_CLASS + ' WHERE idStudent = ' + idStudent, 
			[], 
			function(tx, results) {
				log("Select " + KEY_STUDENT_CLASS + "!");
				if(results.rows.length > 0) {
					callBackFunction(results.rows.item(0));
				} else {
					callBackFunction(null);
				}
			}, 
			function(tx, error) {
				log("Select " + KEY_STUDENT_CLASS + " error: " + error.message);
				callBackFunction(null);
			}
		);
	});
}

function getSClassFromClass(idClass, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'SELECT * FROM ' + KEY_STUDENT_CLASS + ' WHERE idClass = ' + idClass, 
			[], 
			function(tx, results) {
				log("Select " + KEY_STUDENT_CLASS + "!");

				var sClasses = [];
				for(var i = 0; i < results.rows.length; i++) {
					sClasses.push(results.rows.item(i));
				}

				callBackFunction(sClasses);
			}, 
			function(tx, error) {
				log("Select " + KEY_STUDENT_CLASS + " error: " + error.message);
				callBackFunction([]);
			}
		);
	});
}

function removeSClassFromTeam(idTeam, callBackFunction) {
	getClassesFromTeam(idTeam, function(classes) {
		var callBacks = classes.length;
		var callBack = function() {
			callBacks--;
			if(callBacks == 0)
				callBackFunction();
		}

		for(c of classes) {
			removeSClass(c.id, callBack);
		}

	});
	
}

function removeSClass(idClass, callBackFunction) {
	db.transaction(function(tx) {
		tx.executeSql(
			'DELETE FROM ' + KEY_STUDENT_CLASS + ' WHERE idClass = ' + idClass, 
			[], 
			function(tx, results) {
				log("Delete " + KEY_STUDENT_CLASS + "!");
				callBackFunction();
			}, 
			function(tx, error) {
				log("Delete " + KEY_STUDENT_CLASS + " error: " + error.message);
				callBackFunction();
			}
		);
	});
}


// Main script

var s1 = new Student("357417", "Felipe");
var s2 = new Student("357418", "Marques");

//clearStorage();

//verifyAvaliableData();

//saveStudents([s1, s2]);
//newStudent = getStudents();

//verifyAvaliableData();
