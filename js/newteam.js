var teamToAdd = null;
var studentsToAdd = [];

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('of-save-team').addEventListener('click', onAddTeam);
    document.getElementById('of-cancel-team').addEventListener('click', onSeeTeams);
    document.getElementById('of-see-teams').addEventListener('click', onSeeTeams);

    doInit();    
});

function doInit() {

	chrome.storage.sync.get(null, function(data) {
		if(Object.keys(data).length > 0 && data["tmpTeamName"] != "") {
			teamName = data["tmpTeamName"];
			teamToAdd = new Team(-1, teamName);
			studentsToAdd = JSON.parse(data["tmpStudents"]);
		}

		onLoadTeam();
	});
}

function onAddTeam() {
	seeProgess();

	saveTeam(teamToAdd, function() {
		for(s of studentsToAdd)
			s.idTeam = teamToAdd.id;
		saveStudents(studentsToAdd, function() {
			showToast("Turma adicionada!", function() {
				onSeeTeams();
			});
		});
	});
}

function onLoadTeam(callBack) {
	var students = studentsToAdd;
	var table = document.getElementById("students-table");

	for(var i = 1; i < students.length; i++) {
		// Create an empty <tr> element and add it to the 1st position of the table:
		var row = table.insertRow(i);

		// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		// Add some text to the new cells:
		cell1.innerHTML = students[i].name;
		cell1.setAttribute("class", "mdl-data-table__cell--non-numeric");
		cell2.innerHTML = students[i].registration;
	}

	var inputName = document.getElementById("input_team_name");
	inputName.innerHTML = teamToAdd.name;

    componentHandler.upgradeElement(inputName);	
	hideProgess();
}

function onSeeTeams() {
	chrome.tabs.getCurrent(function (tab) {
	  chrome.tabs.update(tab.id, {url: chrome.extension.getURL('allteams.html')});
	});
}

function seeProgess() {
	var bar = document.getElementById("of-progress-bar");
	var done = document.getElementById("of-save-team");
	var cancel = document.getElementById("of-cancel-team");
	bar.style.display = "block";
	done.style.display = "none";
	cancel.style.display = "none";
}

function hideProgess() {
	var bar = document.getElementById("of-progress-bar");
	var done = document.getElementById("of-save-team");
	var cancel = document.getElementById("of-cancel-team");
	bar.style.display = "none";
	done.style.display = "block";
	cancel.style.display = "block";
}

function onSeeTeams() {
	log("Do see teams");
    chrome.tabs.update({'url': chrome.extension.getURL('allteams.html')}, function(tab) {});
}