var table = null;
var teamName = "";

chrome.runtime.onMessage.addListener(function(request, sender) {
	if (request.action == "getSource") {
		table = request.source;
	}

	if (request.action == "getSourceName") {
		log(request.source);
		teamName = request.source;
	}
});

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('btn-add-team').addEventListener('click', onAddTeam);
    document.getElementById('btn-see-teams').addEventListener('click', onSeeTeams);
    document.getElementById('btn-do-sippa').addEventListener('click', onDoSippa);
    document.getElementById('btn-auto-fit').addEventListener('click', onAutoFit);

    onInit();    
});

function onInit(){
    // Since we are not providing an action, this is called a toast.
    chrome.tabs.executeScript(null, {
            file: "js/getpagesource.js"
        }, function() {
            if (chrome.runtime.lastError) {
                log('There was an error injecting script : \n' + chrome.runtime.lastError.message);
            }
        }
    );
}

// Alert

function log(status){
    console.log(status);
}

// Actions

function onAddTeam() {
	log("Do add team");
    if(table == null || teamName == null) {
        alert("Para acionar esta opcao, va ate a pagina de frequencia do SIPPA!");
    } else { 
        chrome.storage.sync.set({'tmpStudents': table, 'tmpTeamName': teamName}, function() {
            chrome.tabs.create({'url': chrome.extension.getURL('newteam.html'), selected: true}, function(tab) {});
        });
    }
}

function onSeeTeams() {
	log("Do see teams");
    chrome.tabs.create({'url': chrome.extension.getURL('allteams.html'), selected: true}, function(tab) {});
}

function onDoSippa() {
    log("Do SIPPA");
    chrome.storage.sync.set({'tmpStudents': table, 'tmpTeamName': teamName}, function() {
        chrome.tabs.create({'url': 'https://sistemas.quixada.ufc.br/apps/sippa/', selected: true}, function(tab) {});
    });
}

function onAutoFit() {
    log("Auto fit");

    if(table == null || teamName == null) {
        alert("Para acionar esta opcao, va ate a pagina de frequencia do SIPPA!");
    } else {

        var elements = document.getElementsByClassName("custom-removable");
        for(var i = 0; i < elements.length; i++) {
            elements[i].style.display = "none";
        }

        getTeamFromName(teamName, function(team) {
            getClassesFromTeam(team.id, function(classes) {
                var container = document.getElementById("main-container-popup");
                var title = document.createElement("span");
                title.classList.add("mdl-cell");
                title.classList.add("mdl-cell--4-col");
                title.innerHTML = teamName;
                container.appendChild(title);

                for(var i = 0; i < classes.length; i++) {
                    var mClass = classes[i];
                    var div = document.createElement("a");
                    div.href = "#";
                    div.classList.add("mdl-cell");
                    div.classList.add("mdl-cell--4--col");
                    div.classList.add("mdl-shadow--2dp");
                    div.classList.add("mdl-color--white");
                    div.classList.add("mdl-color-text--grey-700");
                    div.classList.add("custom-class-item");
                    div.name = JSON.stringify(mClass);
                    div.addEventListener('click', function(e) {
                        autoFitTo(JSON.parse(e.srcElement.name));
                    })
                    div.innerHTML = "Frequencia do dia " + mClass.date + ": " + mClass.plane;
                    container.appendChild(div);
                }
            });
        });
    }
}

function autoFitTo(classToFill) {
    log("Do autofit");
    getStudentsFromClass(classToFill.id, function (students) {
        var studentsJson = JSON.stringify(students);
        var classJson = JSON.stringify(classToFill);

        chrome.tabs.executeScript(null, {
                file: "js/autofill.js"
            }, function() {
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                    if (chrome.runtime.lastError) {
                        log('There was an error injecting script : \n' + chrome.runtime.lastError.message);
                    } else {
                        chrome.tabs.sendMessage(tabs[0].id, {
                            action: "getSourceStudents",
                            source: studentsJson
                        }, function() {});
                        chrome.tabs.sendMessage(tabs[0].id, {
                            action: "getSourceClass",
                            source: classJson
                        }, function() {});
                    }
                });
            }
        );
    });
}

function getStudentsFromClass(classId, callBackFunction) {
    getSClassFromClass(classId, function(sClasses) {
        getStudents(function(students) {
            cStudents = [];
            for(s of students) {
                for(sc of sClasses) {
                    if(sc.idStudent == s.id) {
                        cStudents.push(s);
                    }
                }
            }

            callBackFunction(cStudents);
        });
    })
}