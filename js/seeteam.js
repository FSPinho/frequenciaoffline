var currentTeam = null;

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('of-add-frequency').addEventListener('click', onAddFrequency);
    document.getElementById('checkbox-top').addEventListener('click', onCheckAll);
    document.getElementById('of-delete-selected').addEventListener('click', onDeleteSelected);
    document.getElementById('of-see-teams').addEventListener('click', onSeeTeams);

    doInit();    
});

function doInit() {
	chrome.storage.sync.get(null, function(data) {
		if(Object.keys(data).length > 0 && data["tmpTeamToSee"] != "") {
			teamId = data["tmpTeamToSee"];
			onLoadTeam(teamId);
		}
	});
}

function onLoadTeam(teamId) {
	getTeam(teamId, function(team) {
		currentTeam = team;
		var name = document.getElementById("team-title");
		name.innerHTML = team.name;
	});

	getClassesFromTeam(teamId, function(classes) {
		var table = document.getElementById("teams-table");

		for(var i = 0; i < classes.length; i++) {
			// Create an empty <tr> element and add it to the 1st position of the table:
			var row = table.insertRow(i+1);

			// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);

			cell1.classList.add("mdl-data-table__cell--non-numeric");
			cell1.classList.add("mdl-js-ripple-effect");
			cell1.classList.add("custon-content");
			
			var ripple = document.createElement('span');
			ripple.classList.add("mdl-ripple");
			cell1.appendChild(ripple);

			var link = document.createElement("a");
			link.classList.add("mdl-color-text--grey-700");
			link.href="#";
			link.style.textDecoration = "none";
			link.style.height = "100%";
			cell1.id = classes[i].id;
			link.id = classes[i].id;
			link.addEventListener('click', function(e) { onSeeClass(e.srcElement.id); });
			cell1.addEventListener('click', function(e) { onSeeClass(e.srcElement.id); });
			link.innerHTML = classes[i].date;
			cell1.appendChild(link);

			// Add some text to the new cells:
			cell1.classList.add("mdl-data-table__cell--non-numeric");
			cell2.innerHTML = classes[i].plane;

			var cell2 = row.insertCell(0);
			var htmlString = "<label class=\"mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect\" for=\"checkbox-" + classes[i].id + "\"> <input type=\"checkbox\" id=\"checkbox-" + classes[i].id + "\" class=\"mdl-checkbox__input\"/> </label>";
			cell2.innerHTML = htmlString;
		}

		setTimeout(function() {
          	componentHandler.upgradeDom();
        }, 0);

	});
	
}

function onCheckAll() {
	var table = document.getElementById("teams-table");
	
	var fCell = table.rows[0].cells[0];
	value = hasClass(fCell.getElementsByClassName("mdl-checkbox")[0], "is-checked");
	checked = fCell.getElementsByClassName("mdl-checkbox__input")[0].checked;

	for(var i = 1; i < table.rows.length; i++) {
		console.log("On check all: " + checked);
		var row = table.rows[i];
		var cell = row.cells[0];
		var cbl = cell.getElementsByClassName("mdl-checkbox")[0];
		var cbi = cell.getElementsByClassName("mdl-checkbox__input")[0];

		if(value) {
			if(cbl.className.indexOf("is-checked") == -1)
				cbl.className = cbl.className + " is-checked";
			cbi.checked = true;
		}
		else {
			cbl.className = cbl.className.replace(" is-checked", "");
			cbi.checked = false;
		}
	}

	setTimeout(function() {
      	componentHandler.upgradeDom();
    }, 0);
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function onAddFrequency() {
	log("Add frequency");

	chrome.storage.sync.set({'tmpTeamToFrequency': currentTeam.id, 'tmpClassToFrequency': -1}, function() {
        chrome.tabs.getCurrent(function (tab) {
			chrome.tabs.update(tab.id, {url: chrome.extension.getURL('newclass.html')}, function(tab) {});
		});
    });
	
}

function onDeleteSelected() {
	var table = document.getElementById("teams-table");
	
	var idsToRemove = [];

	for(var i = 1; i < table.rows.length; i++) {
		var row = table.rows[i];
		var cell = row.cells[0];
		var checked = cell.getElementsByClassName("mdl-checkbox__input")[0].checked;
		var id = cell.getElementsByClassName("mdl-checkbox__input")[0].id.replace("checkbox-", "");

		if(checked) {
			table.deleteRow(i--);
			idsToRemove.push(id);
		}
	}

	deleteClass(idsToRemove);

	setTimeout(function() {
      	componentHandler.upgradeDom();
    }, 0);
}

function deleteClass(idsToRemove) {
	for(id of idsToRemove) {
		removeClass(id, function(){});
	}
}

function onSeeClass(id) {
	log("See class: " + id);
	chrome.storage.sync.set({'tmpTeamToFrequency': currentTeam.id, 'tmpClassToFrequency': id}, function() {
        chrome.tabs.getCurrent(function (tab) {
			chrome.tabs.update(tab.id, {url: chrome.extension.getURL('newclass.html')}, function(tab) {});
		});
    });
}

function onSeeTeams() {
	log("Do see teams");
    chrome.tabs.update({'url': chrome.extension.getURL('allteams.html')}, function(tab) {});
}